package az.ingress.home.work.loans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoansMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoansMsApplication.class, args);
    }
}
