package az.ingress.home.work.loans.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "loans")
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double amount;
}
